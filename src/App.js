import { useState } from "react"

function App() {
  const [text, setText] = useState()
  const [showText, setShowText] = useState(false)
  function toggleText() {
    if(!showText) {
    setShowText(true)
    setText('Mario')
    } else {
      setShowText(false)
      setText('Lucho')
    }
  }
 
  return (
    <div>
      <button onClick={ toggleText }> Click Me </button>
      <p> { text } </p>
    </div>
  )
}

export default App
